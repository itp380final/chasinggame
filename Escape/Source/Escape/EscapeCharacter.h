// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "EscapeCharacter.generated.h"

UCLASS(config=Game)
class AEscapeCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;


public:
	AEscapeCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	void BeginPlay() override;
	void StartAttack();
	void StopAttack();
	virtual void Tick(float deltatime) override;
	// Called to taking damage from zombie
	virtual float TakeDamage(float Damage,
		FDamageEvent const& DamageEvent,
		AController* EventInstigator,
		AActor* DamageCauser) override;

	// Called to destroy
	void Dead();

	// Called to show the winning hud
	void SetWin();
protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void EnableLeftClick();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

protected:
	UPROPERTY(EditAnywhere, Category = Weapon)
		TSubclassOf<class AWeapon> WeaponClass;

	UPROPERTY(EditDefaultsOnly, Category = Anime)
	class UAnimMontage* AttackAnim;

	UPROPERTY(EditDefaultsOnly, Category = Anime)
	class UAnimMontage* GetHitAnim;

	UPROPERTY(EditDefaultsOnly, Category = Anime)
	class UAnimMontage* DeadAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStatus)
		float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStatus)
		bool IsDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStatus)
		bool IsWin;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* AttackSound;
	
	UPROPERTY(Transient)
		class UAudioComponent* AttackAC;


private:
	class AWeapon* MyWeapon;
	FTimerHandle  PlayerTimer;
	FTimerHandle DeadTimer;
	float time;
	bool IsAttacking;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

