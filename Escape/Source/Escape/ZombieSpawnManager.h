// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ZombieSpawnManager.generated.h"

UCLASS()
class ESCAPE_API AZombieSpawnManager : public AActor
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere)
	TArray<class ATargetPoint*> SpawnPoints;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<ACharacter> CharacterClass;

	UPROPERTY(EditAnywhere)
	float SpawnTime = 0.5f;

public:	
	// Sets default values for this actor's properties
	AZombieSpawnManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called when all characters have spawned
	void SendSpawnCharacters();

	void OnSpawnTimer();

private:
	TArray<class ACharacter*> Characters;
};
