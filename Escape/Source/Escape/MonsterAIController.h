// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Perception/AISenseConfig_Sight.h"
#include "MonsterAIController.generated.h"
/**
*
*/
UCLASS()
class ESCAPE_API AMonsterAIController : public AAIController
{
	GENERATED_BODY()
public:
	enum State { Sensed, NotSensed, Attack };
	virtual void BeginPlay() override;
	virtual void Tick(float deltasecond) override;
	virtual void OnMoveCompleted(FAIRequestID RequestID,
		const FPathFollowingResult & Result) override;
	void ChasePlayer();
	void Stop();
	UAISenseConfig_Sight* sightConfig;
	TScriptDelegate<> sense;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		float agroRange;

	
	static FORCEINLINE bool VTraceSphere(
		AActor* ActorToIgnore,
		const FVector& Start,
		const FVector& End,
		const float Radius,
		FHitResult& HitOut,
		ECollisionChannel TraceChannel = ECC_Camera
	) {
		FCollisionQueryParams TraceParams(FName(TEXT("VictoreCore Trace")), true, ActorToIgnore);
		TraceParams.bTraceComplex = true;
		//TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = false;

		//Ignore Actors
		TraceParams.AddIgnoredActor(ActorToIgnore);

		//Re-initialize hit info
		HitOut = FHitResult(ForceInit);

		//Get World Source
		TObjectIterator< APlayerController > ThePC;
		if (!ThePC) return false;


		return ThePC->GetWorld()->SweepSingleByChannel(
			HitOut,
			Start,
			End,
			FQuat(),
			TraceChannel,
			FCollisionShape::MakeSphere(Radius),
			TraceParams
		);

	}
private:
	UAIPerceptionComponent* PerceptionComponent;
	enum State state;
	APawn* player0;
	float originSpeed;
	bool isPlaying;
	int check;
	FVector lastloc;
};
