// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Escape.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "EscapeCharacter.h"
#include "Weapon.h"
#include "EscapeGameMode.h"
#include "ZombieCharacter.h"
#include "Sound/SoundCue.h"
//////////////////////////////////////////////////////////////////////////
// AEscapeCharacter

AEscapeCharacter::AEscapeCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	MyWeapon = nullptr;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AEscapeCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AEscapeCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AEscapeCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AEscapeCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AEscapeCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AEscapeCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AEscapeCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AEscapeCharacter::OnResetVR);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AEscapeCharacter::StartAttack);
}


void AEscapeCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AEscapeCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void AEscapeCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void AEscapeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AEscapeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AEscapeCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AEscapeCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AEscapeCharacter::Tick(float deltatime) {
	Super::Tick(deltatime);
	if (this->GetActorLocation().X <-13822.992188 && this->GetActorLocation().X > -14529.992188 &&
		this->GetActorLocation().Y >-4983.995117 && this->GetActorLocation().Y < -3967.0
		&& !IsWin) {
		UE_LOG(LogTemp, Log, TEXT("I HAVE ARRIVED"));
		SetWin();
	}
}

void AEscapeCharacter::BeginPlay() {
	Super::BeginPlay();
	if (WeaponClass)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Need to set rotation like this because otherwise gun points down
			FRotator Rotation(0.0f, 0.0f, 0.0f);
			// Spawn the Weapon
			MyWeapon = World->SpawnActor<AWeapon>(WeaponClass, FVector::ZeroVector,
				Rotation, SpawnParams);
			if (MyWeapon)
			{
				// This is attached to "WeaponPoint" which is defined in the skeleton
				MyWeapon->AttachToComponent(GetMesh(),
					FAttachmentTransformRules::KeepRelativeTransform,
					TEXT("WeaponPoint"));
			}
		}
	}
	
}

void AEscapeCharacter::StartAttack() {
	if (IsAttacking == true) {
		return;
	}
	else {
		IsAttacking = true;
	}
	float duration = PlayAnimMontage(AttackAnim);
	GetWorldTimerManager().SetTimer(PlayerTimer, this, &AEscapeCharacter::EnableLeftClick,
		duration, false);
	TArray<class ACharacter*> zombies = Cast<AEscapeGameMode>(this->GetWorld()->GetAuthGameMode())->GetSpawnCharacters();
	for (int i = 0; i < zombies.Num(); i++) {
		AZombieCharacter* zombie = (Cast<AZombieCharacter>) (zombies[i]);
		FVector zombiepos = zombie->GetActorLocation();
		FVector player = this->GetActorLocation();
		FVector direct = zombiepos - player;
		FRotator zombierot = zombie->GetActorRotation();
		FVector facingdirect = zombierot.Vector();
		direct.Normalize();
		facingdirect.Normalize();
		float angle = FMath::Acos(FVector::DotProduct(direct, facingdirect));
		float distance = FVector::Dist(player, zombiepos);
		if (distance < 200 ) {//&& angle < 1.5707963
			UE_LOG(LogTemp, Log, TEXT("Character Applying Damage"));
			zombie->TakeDamage(10.0f,FDamageEvent(),GetInstigatorController(),	this);
			AttackAC = UGameplayStatics::SpawnSoundAttached(AttackSound, RootComponent);
		}
	}
}

void AEscapeCharacter::EnableLeftClick() {
	IsAttacking = false;
}

void AEscapeCharacter::StopAttack() {
	StopAnimMontage(AttackAnim);
}

float AEscapeCharacter::TakeDamage(float Damage,
	FDamageEvent const& DamageEvent,
	AController* EventInstigator,
	AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.0f) {
		Health -= ActualDamage;

		UE_LOG(LogTemp, Log, TEXT("Attacked !!!, %f"), Health);
		if (Health <= 0.0f)
		{ // We're dead, don't allow further damage 
			bCanBeDamaged = false;
			// TODO: Process death 
			StopAttack();
			time = PlayAnimMontage(DeadAnim) - 0.25f;
			GetWorldTimerManager().SetTimer(DeadTimer, this, &AEscapeCharacter::Dead, time, false);

		}
	}
	return ActualDamage;
}

void AEscapeCharacter::SetWin() {
	IsWin = true;
}

void AEscapeCharacter::Dead() {
	//Destroy();
}