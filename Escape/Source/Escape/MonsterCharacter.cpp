// Fill out your copyright notice in the Description page of Project Settings.

#include "Escape.h"
#include "MonsterCharacter.h"
#include "MonsterAIController.h"
#include "Sound/SoundCue.h"

// Sets default values
AMonsterCharacter::AMonsterCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AIControllerClass = AMonsterAIController::StaticClass();

	damage = 1.0f;
}

// Called when the game starts or when spawned
void AMonsterCharacter::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMonsterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMonsterCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void AMonsterCharacter::Kill() {
	if (UGameplayStatics::GetPlayerPawn(this, 0) != nullptr) {
		UGameplayStatics::GetPlayerPawn(this, 0)->TakeDamage(damage, FDamageEvent(), GetInstigatorController(), this);
	}
}

void AMonsterCharacter::PlaySound() {
	MonsterAC = UGameplayStatics::SpawnSoundAttached(MonsterSound, RootComponent);
}

void AMonsterCharacter::StopSound() {
	MonsterAC->Stop();
}