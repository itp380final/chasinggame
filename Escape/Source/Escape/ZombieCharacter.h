// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "ZombieCharacter.generated.h"

UCLASS()
class ESCAPE_API AZombieCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* AttackAnim;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* JumpAttackAnim;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* DeadAnim;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* GetHitAnim;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* WonderAnim;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* ChaseAnim;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* IdleAnim;

	UPROPERTY(EditAnywhere, Category = Health)
	float Health = 20.0f;

	UPROPERTY(EditAnywhere, Category = Damage)
	float Damage = 0.2f;

public:
	// Sets default values for this character's properties
	AZombieCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// Called to start attack
	void StartAttack();

	// Called to stop attack
	void StopAttack();

	// Called to chase
	void StartChase();

	// Called to idle
	void StopChase();
	
	// Called to wonder
	void StartWonder();

	// Called to stop wonder;
	void StopWonder();
	
	// Called to idle
	void StartIdle();

	// Called to stop idle
	void StopIdle();

	// Called to recover
	void Recover();
	
	// Called to make it dead
	void Dead();

	

	// Called to start stop getting hit
	void StopGetHit();

	// Called to making damage on character
	void DamageOnCharacter();
	
	// Called to taking damage from character
	virtual float TakeDamage(float Damage,
		FDamageEvent const& DamageEvent,
		AController* EventInstigator,
		AActor* DamageCauser) override;


private:
	APawn* player;
	int attackactionPerformed;
	FTimerHandle AttackTimer;
	FTimerHandle DeadTimer;
	FTimerHandle GetHitTimer;
	bool isDead = false;
	float time;
};
