// Fill out your copyright notice in the Description page of Project Settings.

#include "Escape.h"
#include "ZombieSpawnManager.h"
#include "Engine/TargetPoint.h"
#include "ZombieAIController.h"
#include "EscapeGameMode.h"
// Sets default values
AZombieSpawnManager::AZombieSpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AZombieSpawnManager::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle timehandle1;
	GetWorldTimerManager().SetTimer(timehandle1, this, &AZombieSpawnManager::OnSpawnTimer, SpawnTime, false);
	FTimerHandle timehandle2;
	GetWorldTimerManager().SetTimer(timehandle2, this, &AZombieSpawnManager::SendSpawnCharacters, SpawnTime+1, false);
}

// Called every frame
void AZombieSpawnManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AZombieSpawnManager::OnSpawnTimer() {
	//int num = SpawnPoints.Num();
	for (int i = 0; i < SpawnPoints.Num(); i++) {
		int index =	i;
		FVector pos = SpawnPoints[index]->GetActorLocation();
		FRotator rot = SpawnPoints[index]->GetActorRotation();

		ACharacter* Char = GetWorld()->SpawnActor<ACharacter>(CharacterClass, pos, rot);
		if (Char) {
			Char->SpawnDefaultController();
			((Cast<AZombieAIController>)(Char->GetController()))->SetSpawnLocation(pos);
			Characters.Add(Char);
		}
	}
}

void AZombieSpawnManager::SendSpawnCharacters() {
	(Cast<AEscapeGameMode>)(this->GetWorld()->GetAuthGameMode())->SetSpawnCharacters(Characters);
}