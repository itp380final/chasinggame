// Fill out your copyright notice in the Description page of Project Settings.

#include "Escape.h"
#include <iostream>
#include "ZombieAIController.h"
#include "ZombieCharacter.h"
#include "Engine/TargetPoint.h"

void AZombieAIController::BeginPlay() {
	Super::BeginPlay();
	UE_LOG(LogTemp, Log, TEXT("Spawned"));

	player0 = UGameplayStatics::GetPlayerPawn(this, 0);
	//	spawnlocation = this->GetPawn()->GetActorLocation();

	//MoveToActor(player0);
	state = Idle;
	attackRange = 150.0f;
	alertRange = 1500.0f;
}

void AZombieAIController::Tick(float deltasecond) {
	if (GetPawn() != nullptr) {
		if (state == Idle) {
			if (UGameplayStatics::GetPlayerPawn(this, 0) != nullptr) {
				FVector player = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
				FVector zombie = GetPawn()->GetActorLocation();
				float distance = FVector::Dist(player, zombie);
				if (distance < alertRange) {
					(Cast<AZombieCharacter>(GetPawn()))->StopIdle();
					ChasePlayer();
				}
			}
			else {
				(Cast<AZombieCharacter>(GetPawn()))->StartIdle();
			}
		}
		else if (state == Attack) {
			if (UGameplayStatics::GetPlayerPawn(this, 0) != nullptr) {
				FVector player = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
				FVector zombie = GetPawn()->GetActorLocation();
				
				float distance = FVector::Dist(player, zombie);
				if ((distance > attackRange) && (distance < alertRange)) {
					ChasePlayer();
				}
				else if (distance >= alertRange) {
					MoveToStart();
				}
			}
		}
		
	}
}

void AZombieAIController::ChasePlayer() {
	state = Chase;
	(Cast<AZombieCharacter>(GetPawn()))->StopAttack();
	(Cast<AZombieCharacter>(GetPawn()))->StartChase();
	MoveToActor(player0);
}

void AZombieAIController::MoveToStart() {
	state = Idle;
	(Cast<AZombieCharacter>(GetPawn()))->StopChase();
	(Cast<AZombieCharacter>(GetPawn()))->StartWonder();
	MoveToLocation(spawnlocation);
}

void AZombieAIController::OnMoveCompleted(FAIRequestID RequestID,
	const FPathFollowingResult & Result) {
	if (Result.IsSuccess()) {
		if (state == Chase) {
			FVector player = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
			FVector zombie = GetPawn()->GetActorLocation();
			float distance = FVector::Dist(player, zombie);
			if(distance < attackRange){
				state = Attack;
				(Cast<AZombieCharacter>(GetPawn()))->StopChase();
				(Cast<AZombieCharacter>(GetPawn()))->StartAttack();
			}
		} if (state == Idle) {
			state = Idle;
			(Cast<AZombieCharacter>(GetPawn()))->StopChase();
			(Cast<AZombieCharacter>(GetPawn()))->StartIdle();
		}
	}
}

void AZombieAIController::SetSpawnLocation(FVector pos) {
	spawnlocation = pos;
}

void AZombieAIController::SetDead() {
	state = Dead;
}
void AZombieAIController::SetRecover() {
	state = Alert;
}