// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Escape.h"
#include "EscapeGameMode.h"
#include "EscapeCharacter.h"

AEscapeGameMode::AEscapeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AEscapeGameMode::SetSpawnCharacters(TArray<class ACharacter*> Characters) {
	this->Characters = Characters;
}

TArray<class ACharacter*> AEscapeGameMode::GetSpawnCharacters() {
	return Characters;
}