// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "EscapeGameMode.generated.h"

UCLASS(minimalapi)
class AEscapeGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AEscapeGameMode();
	void SetSpawnCharacters(TArray<class ACharacter*> Characters);
	TArray<class ACharacter*> GetSpawnCharacters();
private:
	TArray<class ACharacter*> Characters;
};



