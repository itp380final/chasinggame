// Fill out your copyright notice in the Description page of Project Settings.

#include "Escape.h"
#include <stdlib.h>
#include <iostream>
#include "ZombieCharacter.h"
#include "ZombieAIController.h"

// Sets default values
AZombieCharacter::AZombieCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AIControllerClass = AZombieAIController::StaticClass();
	player = UGameplayStatics::GetPlayerPawn(this, 0);

}

// Called when the game starts or when spawned
void AZombieCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZombieCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (UGameplayStatics::GetPlayerPawn(this, 0) != nullptr && this != nullptr && !isDead) {
		FVector player = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
		FVector zombie = this->GetActorLocation();
		FVector dirc = player - zombie;
		dirc.Normalize();
		FRotator rot = dirc.Rotation();
		rot.Pitch = 0;
		this->SetActorRotation(rot);
	}
}

// Called to bind functionality to input
void AZombieCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

// Called to start attack
void AZombieCharacter::StartAttack() {
	if (this != nullptr) {
		attackactionPerformed = rand()%5;
		
		if(attackactionPerformed == 4){
			UE_LOG(LogTemp, Log, TEXT("I am Jump Attacking"));
			float time = PlayAnimMontage(JumpAttackAnim);
			GetWorldTimerManager().SetTimer(AttackTimer, this, &AZombieCharacter::DamageOnCharacter, time, true);
		} else {
			UE_LOG(LogTemp, Log, TEXT("I am Normal Attacking"));
			float time = PlayAnimMontage(AttackAnim);
			GetWorldTimerManager().SetTimer(AttackTimer, this, &AZombieCharacter::DamageOnCharacter, time, true);
		}
	}
}

// Called to stop Attack
void AZombieCharacter::StopAttack() {
	if (this != nullptr) {
		if (attackactionPerformed == 4) {
			StopAnimMontage(JumpAttackAnim);
			GetWorldTimerManager().ClearTimer(AttackTimer);
		} else {
			StopAnimMontage(AttackAnim);
			GetWorldTimerManager().ClearTimer(AttackTimer);
		}
	}
}

// Called to chase
void AZombieCharacter::StartChase() {
	if (this != nullptr) {
		UE_LOG(LogTemp, Log, TEXT("I am Chasing"));
		PlayAnimMontage(ChaseAnim);
	}
}

// Called to stop chase
void AZombieCharacter::StopChase() {
	if (this != nullptr) {
		StopAnimMontage(ChaseAnim);
	}
}

// Called to wonder
void AZombieCharacter::StartWonder() {
	if (this != nullptr) {
		UE_LOG(LogTemp, Log, TEXT("I am Moving back"));
		PlayAnimMontage(WonderAnim);
	}
}

// Called to stop wonder;
void AZombieCharacter::StopWonder() {
	if (this != nullptr) {
		StopAnimMontage(WonderAnim);
	}
}


// Called to idle
void AZombieCharacter::StartIdle() {
	if (this != nullptr) {
		PlayAnimMontage(IdleAnim);
	}
}

// Called to stop idle
void AZombieCharacter::StopIdle() {
	if (this != nullptr) {
		StopAnimMontage(IdleAnim);
	}
}

// Called to stop getting hit
void AZombieCharacter::StopGetHit() {
	if (this != nullptr) {
		StopAnimMontage(GetHitAnim);
		(Cast<AZombieAIController>)(this->GetController())->ChasePlayer();
		//StartAttack();
		//GetWorldTimerManager().SetTimer(GetHitTimer, this, &AZombieCharacter::StartAttack, time, false);
	}
}

// Called to play dead
void AZombieCharacter::Dead() {
	if (this != nullptr) {
		GetWorldTimerManager().ClearTimer(DeadTimer);
		isDead = true;
		time += 200.25f;
		Health = 20.0f;
		FTimerHandle recoverTimer;
		UE_LOG(LogTemp, Log, TEXT("Zombie Dead"));
		GetWorldTimerManager().SetTimer(recoverTimer, this, &AZombieCharacter::Recover, time, false);
	}
}

// Called to alive
void AZombieCharacter::Recover() {
	//bCanBeDamaged = true;
	//UE_LOG(LogTemp, Log, TEXT("Zombie Recover"));
	Destroy();
	//(Cast<AZombieAIController>)(GetController())->SetRecover();
}

// Called to taking damage on character
void AZombieCharacter::DamageOnCharacter() {
	if (UGameplayStatics::GetPlayerPawn(this, 0) != nullptr) {
		UGameplayStatics::GetPlayerPawn(this, 0)->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
	}
}

float AZombieCharacter::TakeDamage(float Damage,
	FDamageEvent const& DamageEvent,
	AController* EventInstigator,
	AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.0f) {
		Health -= ActualDamage;
		if (Health <= 0.0f)
		{ // We're dead, don't allow further damage 
			bCanBeDamaged = false;
			// TODO: Process death 
			StopAttack();
			//GetController()->UnPossess();
			(Cast<AZombieAIController>)(GetController())->SetDead();
			time = PlayAnimMontage(DeadAnim) - 0.25f;
			GetWorldTimerManager().SetTimer(DeadTimer, this, &AZombieCharacter::Dead, time, false);
			
		}
		else {
			UE_LOG(LogTemp, Log, TEXT("Zombie Damage Taken"));
			StopAttack();
			time = PlayAnimMontage(GetHitAnim)-0.25f;
			GetWorldTimerManager().SetTimer(GetHitTimer, this, &AZombieCharacter::StopGetHit, time, false);
		}
	}
	return ActualDamage;
}