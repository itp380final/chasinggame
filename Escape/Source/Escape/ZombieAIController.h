// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "ZombieAIController.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPE_API AZombieAIController : public AAIController
{
	GENERATED_BODY()
public:
	enum State {Idle, Alert, Chase, Attack, Dead};
	virtual void BeginPlay() override;
	virtual void Tick(float deltasecond) override;
	void ChasePlayer();
	void MoveToStart();
	virtual void OnMoveCompleted(FAIRequestID RequestID,
		const FPathFollowingResult & Result) override;
	void SetSpawnLocation(FVector pos);
	void SetDead();
	void SetRecover();

private:
	enum State state;
	APawn* player0;
	float attackRange;
	float alertRange;
	float chasingRange;
	FVector spawnlocation;
};
