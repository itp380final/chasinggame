// Fill out your copyright notice in the Description page of Project Settings.

#include "Escape.h"
#include "MonsterAIController.h"
#include "MonsterCharacter.h"
#include "Kismet/KismetSystemLibrary.h"
#include <math.h>
#include "Perception/AIPerceptionComponent.h"


void AMonsterAIController::BeginPlay() {
	Super::BeginPlay();
	player0 = UGameplayStatics::GetPlayerPawn(this, 0);
	state = NotSensed;
	originSpeed = (Cast<AMonsterCharacter>(GetPawn()))->GetCharacterMovement()->MaxWalkSpeed;
	lastloc = GetPawn()->GetActorLocation();
	isPlaying = false;
	check = 2000;
}

void AMonsterAIController::Tick(float deltasecond) {
	if (GetPawn() != nullptr) {
		FVector player = player0->GetActorLocation();
		FVector monster = GetPawn()->GetActorLocation();
		FVector dirc = player - monster;
		float dist = FVector::Dist(player, monster);
		if (dist < 1200.0 && !isPlaying) {
			(Cast<AMonsterCharacter>(GetPawn()))->PlaySound();
			isPlaying = true;
		}
		else if (isPlaying && dist > 1200.0) {
			(Cast<AMonsterCharacter>(GetPawn()))->StopSound();
			isPlaying = false;
		}
		dirc.Normalize();
		if (UGameplayStatics::GetPlayerPawn(this, 0) != nullptr && this != nullptr) {
			FRotator rot = dirc.Rotation();
			rot.Pitch = 0;
			GetPawn()->SetActorRotation(rot);
		}
		FVector Vforward = player0->GetActorForwardVector();
		Vforward.Normalize();
		dirc = monster - player;
		dirc.Normalize();
		float dot = FVector::DotProduct(Vforward, dirc);
		float angle = FGenericPlatformMath::Acos(dot);
		FHitResult HitOut;
		ECollisionChannel TraceChannel;
		if (VTraceSphere(this, player0->GetActorLocation(), GetPawn()->GetActorLocation(), 80, HitOut, TraceChannel = ECC_Camera) && (angle < (PI / (float)4))) {
			if (state == NotSensed) {
				UE_LOG(LogTemp, Log, TEXT("Stop"));
				Stop();
			}
		}
		else {
			if (state == Sensed) {
				UE_LOG(LogTemp, Log, TEXT("Chase"));
				ChasePlayer();
			}
		}
	}
}

void AMonsterAIController::ChasePlayer() {
	state = NotSensed;
	(Cast<AMonsterCharacter>(GetPawn()))->GetCharacterMovement()->MaxWalkSpeed *= 3;
	MoveToActor(player0);
}

void AMonsterAIController::Stop() {
	state = Sensed;
	(Cast<AMonsterCharacter>(GetPawn()))->GetCharacterMovement()->MaxWalkSpeed = originSpeed;
	StopMovement();
}

void AMonsterAIController::OnMoveCompleted(FAIRequestID RequestID,
	const FPathFollowingResult & Result) {
	if (Result.IsSuccess()) {
		FVector player = player0->GetActorLocation();
		FVector monster = GetPawn()->GetActorLocation();
		float dist = FVector::Dist(player, monster);
		if(dist<150){
			state = NotSensed;
			(Cast<AMonsterCharacter>(GetPawn()))->Kill();
		}
	}
}
