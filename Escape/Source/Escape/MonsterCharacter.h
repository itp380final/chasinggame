// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MonsterCharacter.generated.h"

UCLASS()
class ESCAPE_API AMonsterCharacter : public ACharacter
{
	GENERATED_BODY()


public:
	// Sets default values for this character's properties
	AMonsterCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// Called to making damage on character
	void Kill();

	void PlaySound();

	void StopSound();

protected:
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* MonsterSound;

	UPROPERTY(Transient)
		class UAudioComponent* MonsterAC;

private:
	float damage;
};
